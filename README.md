# Crowdsource science improvement

To get started, go to mybinder: 

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/jibe-b%2Fcrowdsource-science-improvement/dev?urlpath=lab%2Ftree%2Fcrowdsource-science-improvement-tuto.ipynb)
